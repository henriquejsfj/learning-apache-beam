import apache_beam as beam


class MergeInList(beam.CombineFn):
    """Beam CombineFn to group all elements inside a single list"""
    def create_accumulator(self):
        return [], None

    def add_input(self, prev, input_):
        prev[0].append(input_)
        return prev

    def merge_accumulators(self, accumulators):
        r = []
        for accum in accumulators:
            r += accum[0]
        return r, None

    def extract_output(self, result):
        return result[0]
