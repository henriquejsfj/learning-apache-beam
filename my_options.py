from apache_beam.options.pipeline_options import PipelineOptions

class MyOptions(PipelineOptions):
    """Beam PipelineOptions receive input files and the output directory"""

    @classmethod
    def _add_argparse_args(cls, parser):
        parser.add_argument(
            '--input_covid',
            type=str,
            help='CSV file with covid data',
            default='HIST_PAINEL_COVIDBR_28set2020.csv'
        )
        parser.add_argument(
            '--input_states',
            type=str,
            help='CSV file with IBGE states data',
            default='EstadosIBGE.csv'
        )
        parser.add_argument(
            '--output',
            type=str,
            help='Directory to receive the output files',
            default='output/'
        )
