import apache_beam as beam
from numpy import sum as np_sum
from numpy import array as arr


class SumListItems(beam.CombineFn):
    """Beam CombineFn to sum the provided indexes in a list (row)"""
    def __init__(self, *items):
        super().__init__()
        self.__items = items

    def create_accumulator(self):
        return [[0 for i in self.__items], None]

    def add_input(self, prev, input_):
        if prev[1] is None:
            prev[1] = input_
        for i, index in enumerate(self.__items):
            prev[0][i] += int(input_[index])
        return prev

    def merge_accumulators(self, accumulators):
        adders, _ = zip(*accumulators)
        return np_sum(adders, axis=0), accumulators[0][1]

    def extract_output(self, result):
        for i, index in enumerate(self.__items):
            result[1][index] = str(result[0][i])
        return result[1]
