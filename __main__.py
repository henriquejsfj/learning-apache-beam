import json
from os.path import join

import apache_beam as beam
from apache_beam import coders
from apache_beam.io.textio import WriteToText

from my_options import MyOptions
from parse_csv import ParseCSV
from sum_list_items import SumListItems
from merge_in_list import MergeInList

HEADER = ["Regiao", "Estado", "UF", "Governador", "TotalCasos", "TotalObitos"]

COD_UF = [11, 12, 13, 14, 15, 16, 17, 21, 22, 23, 24, 25, 26, 27, 28, 29, 31, 32, 33, 35, 41, 42, 43, 50, 51, 52, 53]


def printer(arg):
    print(arg, "\n")
    return arg


def format_output_lines(element):
    x = element[0][0]
    y = element[1][0]
    return [x[0], y[0], x[1], y[1], x[2], x[3]]


if __name__ == "__main__":
    my_options = MyOptions()
    output = join(my_options.output, "result")
    with beam.Pipeline(options=my_options) as p:
        covid_lines = p | my_options.input_covid >> ParseCSV()
        states_lines = p | my_options.input_states >> ParseCSV()

        paired_covid = (covid_lines
                        | "Filter out individual cities" >> beam.Filter(lambda x: not x[2])
                        | "Make pair with needed data" >> beam.Map(lambda x: (x[3], [x[0], x[1], x[11], x[13], x[7]]))
                        | "Sum the cases by UF cod" >> beam.CombinePerKey(SumListItems(2, 3))
                        )

        paired_estados = states_lines | "Set UF cod as key" >> beam.Map(lambda x: (x[1], [x[0], x[3]]))

        grouped = ((paired_covid, paired_estados)
                   | "Join both tables by cod UF key" >> beam.CoGroupByKey()
                   | "Discard invalid keys" >> beam.Filter(lambda x, uf: (int(x[0]) in uf), uf=COD_UF)
                   | "Discard cod UF keys" >> beam.Values()
                   | "Rearrange columns for the output" >> beam.Map(format_output_lines)
                   | "print grouped" >> beam.Map(printer)
                   )

        dict_format = (grouped
                       | "Transform table in dict" >>
                            beam.Map(lambda x, keys: {key: value for key, value in zip(keys, x)}, keys=HEADER)
                       | "print dict_format" >> beam.Map(printer)
                       )

        (grouped
         | "Make csv lines" >> beam.Map(";".join)
         | "Save csv file" >> WriteToText(output, ".csv", header=";".join(HEADER))
         )
        (dict_format
         | "Create list containing all" >> beam.CombineGlobally(MergeInList())
         | "Make it a JSON" >> beam.Map(json.dumps, ensure_ascii=False, indent=1)
         | "Save JSON file" >> WriteToText(output, ".json", coder=coders.StrUtf8Coder())
         )
