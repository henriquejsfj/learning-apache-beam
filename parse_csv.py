import csv

import apache_beam as beam
from apache_beam.io.textio import ReadFromText


class ParseCSV(beam.PTransform):
    """Beam PTransform to parse a CSV file, the label is the name of the file"""
    def expand(self, pipeline):
        return pipeline | 'ReadMyFile' >> ReadFromText(self.label, skip_header_lines=1) \
               | 'Parse CSV' >> beam.FlatMap(ParseCSV.parse_csv)

    @staticmethod
    def parse_csv(file):
        return csv.reader([file], quotechar='"', delimiter=';')
