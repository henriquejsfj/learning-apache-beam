# Learning Apache Beam

Learning how to develop data processing pipelines with Apache Beam.

In this project, the objective is to filter and sum up the data of 2 tables. One table contains the Brazilian states
geography information like its area, population, governor, etc. The other contains Brazilian data about the COVID-19
disease, with its total counted cases and death by city and date. Thus, these tables will be merged into one by the
Brazilian states, containing only the current number of cases and deaths besides some geographical info. The output will
be available in both CSV and JSON formats. Here is one example of the JSON output:

``` json
{
  "Regiao": "Norte",
  "Estado": "Acre",
  "UF": "AC",
  "Governador": "GLADSON DE LIMA CAMELI",
  "TotalCasos": "27885",
  "TotalObitos": "657"
}
```

## Usage

The usage of this repo is simple, first clone it:
```commandline
$ git clone https://gitlab.com/henriquejsfj/learning-apache-beam.git
```

Then, go into the project directory:
```commandline
$ cd learning-apache-beam
```

Install the requirements (it is strongly recommended to use a [virtual environment](https://docs.python.org/3/library/venv.html)):
```commandline
$ pip3 install -r requirements.txt
```

Finally run the code (inside the project directory):
```commandline
$ python3 .
```

If you desire to change input file path or the output directory type `--help` to see the help message with directions.

## About the codes

There is a main code called `__main__.py` which uses other modules to help with the pipeline process. A list of the 
modules with a brief description is shown below:
- `my_options` - Beam PipelineOptions receive input files and the output directory.
- `parse_csv` - Beam PTransform to parse a CSV file, the label is the name of the file.
- `sum_list_items` - Beam CombineFn to sum the provided indexes in a list (row).
- `merge_in_list` - Beam CombineFn to group all elements inside a single list.

The pipeline is simple, and better explained by the diagram below:

![diagram](diagram.png)

Briefly, the pipeline parses the file with covid data and remove the rows with data from unique city. Then map the rows
to be keyed bu the UF cod with usefull data and finally group them by the states making the sum of the cases. Later,
the processed data from states table is merged with the processed data from the covid data.
Then, the keys (a code for the states, `UF cod`) is discarded after verifying that they are valid states. 
Finally, the data is rearranged to fit our needs for the output and then follows one branch to save the result in a
CSV file and also has another branch to save in JSON format.